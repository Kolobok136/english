package ru.patyukov;

import static ru.patyukov.Select.*;

public class App {
    public static void main(String[] args) {
        ManagerWords managerWords = new ManagerWords(args[0]);
        Select select = NOT_EXIT;

        while (!select.equals(FIVE)) {
            managerWords.cls();
            select = Select.valid(managerWords.printMenu());

            managerWords.cls();
            switch (select) {
                case ZERO -> managerWords.addWord();
                case ONE -> managerWords.readEnWord();
                case TO -> managerWords.readRuWord();
                case THREE -> managerWords.readAllWord();
                case FOUR -> managerWords.rules();
                case FIVE -> managerWords.out();
            }
        }
    }
}