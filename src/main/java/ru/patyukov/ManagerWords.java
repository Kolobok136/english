package ru.patyukov;

import java.io.*;
import java.util.*;

public class ManagerWords {
    private static final Scanner SCANNER = new Scanner(System.in);
    private static final String RESTART_MENU = "Для возврата в меню ткни enter";
    private static final String NEXT_WORD = "Для перехода к следующему слову ткни enter";
    private static final String LINE = "=========>>> ";

    private String path;
    private String allWords = "";
    private Map<String, String> mapWords = new HashMap<>();

    public ManagerWords(String path) {
        this.path = path;
    }

    public String printMenu() {
        System.out.println("\tАнглийский\n");
        System.out.println("0 - добавить слово");
        System.out.println("1 - английские слова");
        System.out.println("2 - русские слова");
        System.out.println("3 - все слова с переводом");
        System.out.println("4 - правила");
        System.out.println("5 - выход");

        System.out.print("\nввод: ");

        return SCANNER.nextLine();
    }

    public void addWord() {
        read();

        System.out.print("Введи английское слово: ");
        String wordEn = SCANNER.nextLine();

        System.out.print("Введи перевод: ");
        String wordRu = SCANNER.nextLine();

        try(BufferedWriter writer = new BufferedWriter(new FileWriter(path))) {
            writer.write(allWords + wordEn + " - " + wordRu);
            allWords = "";
            mapWords = new HashMap<>();
        } catch (IOException e) {
            System.out.println("Не удалось сохранить слово");
        }
    }

    public void readEnWord() {
        System.out.println("\t" + NEXT_WORD + "\n");

        read();

        mapWords.forEach((k, v) -> {
            System.out.print(k);
            SCANNER.nextLine();
            System.out.print(LINE + v);
            SCANNER.nextLine();
        });

        System.out.print("\n\t" + RESTART_MENU);
        SCANNER.nextLine();

        allWords = "";
        mapWords = new HashMap<>();
    }

    public void readRuWord() {
        System.out.println("\t" + NEXT_WORD + "\n");

        read();

        mapWords.forEach((k, v) -> {
            System.out.print(v);
            SCANNER.nextLine();
            System.out.print(LINE + k);
            SCANNER.nextLine();
        });

        System.out.print("\n\t" + RESTART_MENU);
        SCANNER.nextLine();

        allWords = "";
        mapWords = new HashMap<>();
    }

    public void readAllWord() {
        read();

        System.out.print(allWords);

        allWords = "";
        mapWords = new HashMap<>();

        System.out.print("\n\t" + RESTART_MENU);
        SCANNER.nextLine();
    }

    public void rules() {
        System.out.println("Тут будут правила грамматики");
        System.out.print("\n\t" + RESTART_MENU);
        SCANNER.nextLine();
    }

    public void out() {
        System.out.print("Завершение ");
        for (int i = 0; i < 3; i++) {
            try {
                Thread.sleep(1000);
                System.out.print(".");
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void cls() {
        try {
            new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
        } catch (Exception exp) {
            System.out.println("Очистка консоли сломалась");
        }
    }

    private void read() {
        try(BufferedReader reader = new BufferedReader(new FileReader(path))) {
            reader.lines().forEach(w -> {
                String[] word = w.split("-");
                mapWords.put(word[0].trim(), word[1].trim());
                allWords = allWords + w + "\n";
            });
        } catch (IOException e) {
            System.out.println("Не удалось прочитать файлы");
            throw new RuntimeException(e);
        }
    }
}
