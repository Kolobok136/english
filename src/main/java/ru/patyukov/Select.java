package ru.patyukov;

import java.util.Arrays;

public enum Select {
    ZERO("0"),
    ONE("1"),
    TO("2"),
    THREE("3"),
    FOUR("4"),
    FIVE("5"),
    NOT_EXIT("notExit"),
    ;

    Select(String key) {
        this.key = key;
    }

    private final String key;

    public String getKey() {
        return key;
    }

    public static Select valid(String key) {
        return Arrays.stream(Select.values())
                .filter(select -> select.getKey().equals(key))
                .findAny()
                .orElse(NOT_EXIT);
    }
}
